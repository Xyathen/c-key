# C-KEY README #
### What is this repository for? ###

This repository is designed to be a home for my first C project, where others can contribute and help me learn!

### How do I get set up? ###

Download the source files and create a project in your favorite IDE, and just import the files into your project.

### Contribution guidelines ###

Anything goes, all suggestions, improvements, and tips are welcome.

### Who do I talk to? ###

You can talk to the repo owner for any inquires or contacts.