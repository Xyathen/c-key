/*
  Name: CGEN
  Copyright: Copyright (c) Xyathen 2014
  Author: Xyathen
  Date: 01/11/14 11:29
  Description: A simple C based key generator written for practice and fun.
*/

#include <stdio.h>
#include <string.h>

/*
  Creates a prompt for user input that temporarily stops the program.
*/
void consolePause()
{
     printf("\nPress any key to continue...");
     getch();
}

int main(void)
{
    //Welcome message and username prompt.
    printf("Welcome to CGEN, a C based key generator!\n");
    printf("Username:  ");
    
    //Username string and scan function to get the user's input.
    char username[] = "";
    scanf("%s", &username);
    
    //Serial key integer and generation algorithm.
    int serialKey = strlen(username) * 8000 * 47;
    
    //Displays the user's serial key with proper format.
    printf("Serial:  ");
    printf("%i", serialKey);
    
    //Program end.
    consolePause();
    return 0;
}
